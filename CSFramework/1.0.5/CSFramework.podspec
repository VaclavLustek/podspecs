#
# Be sure to run `pod lib lint CSFramework.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

    s.name          = "CSFramework"
    s.version       = "1.0.5"
    s.summary       = "Custom framework to simplify iOS programming. Contains UI, convenience and network classes."
    s.homepage      = "https://bitbucket.org/Ydus/csframework"
    s.license       = 'MIT'
    s.author        = { "Václav Luštěk" => "lustek@email.cz" }
    s.source        = { :git => "https://bitbucket.org/Ydus/csframework.git", :branch => "master", :tag => s.version.to_s }

    s.platform      = :ios, '7.0'
    s.requires_arc  = true

    s.source_files  = 'CSFramework/**/*.{h,m}'

    s.dependency 'ConstraintFormatter'

end
